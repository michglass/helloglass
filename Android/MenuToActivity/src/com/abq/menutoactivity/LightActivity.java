package com.abq.menutoactivity;

import com.google.android.glass.app.Card;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.KeyEvent;

public class LightActivity extends Activity {
	
	SensorManager lightSensorManager;
	Sensor lightSensor;
	long startTime = System.currentTimeMillis();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		lightSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		lightSensor = lightSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		lightSensorManager.registerListener(lightSensorEventListener, lightSensor, 
				SensorManager.SENSOR_DELAY_NORMAL);
		
		Card lightCard = new Card(this);
		lightCard.setText("LIGHT");
		setContentView(lightCard.toView());
	}
	
	// light sensor listener
	private SensorEventListener lightSensorEventListener = new SensorEventListener() {
		
		
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			
			if(event.sensor.getType() == Sensor.TYPE_LIGHT) {
				
				long millis = System.currentTimeMillis() - startTime;
				int seconds = (int) millis/1000;
				if(seconds == 1) {
					getLight(event);
					startTime = System.currentTimeMillis();
				}
			}
		}
		
	};
	
	// display light information
	private void getLight(SensorEvent event) {
		
		float ambLightValue = event.values[0];
		Card lightCard = new Card(this);
		String lightText = "SI units: " + ambLightValue;
		lightCard.setText(lightText);
		lightCard.setFootnote("swipe down for home");
		setContentView(lightCard.toView());
	}
	
	@Override
	public boolean onKeyDown(int keycode, KeyEvent event) {
		if(keycode == KeyEvent.KEYCODE_BACK) {
			finish();
			return true;
		}
		return false;
	}
}
