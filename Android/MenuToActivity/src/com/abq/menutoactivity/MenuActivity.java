package com.abq.menutoactivity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MenuActivity extends Activity {

	private Intent gyroActivityIntent;
	private Intent lightActivityIntent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Log.v("MENU ACTIVITY", "ON CREATE");
		openOptionsMenu();
		gyroActivityIntent = new Intent(this, GyroActivity.class);
		lightActivityIntent = new Intent(this, LightActivity.class);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuInflater menuInfl = getMenuInflater();
		menuInfl.inflate(R.menu.glass_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch(item.getItemId()) {
			case R.id.showGyroData:
				startActivity(gyroActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				finish();
				return true;
			case R.id.lightData:
				startActivity(lightActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				finish();
				return true;
			case R.id.goBack:
				finish();
		}
		return false;
	}

}
