package com.abq.menutoactivity;

import com.google.android.glass.app.Card;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.KeyEvent;

public class GyroActivity extends Activity {

	SensorManager gyroSensorManager;
	Sensor gyroSensor;
	long startTime = System.currentTimeMillis();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		gyroSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		gyroSensor = gyroSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		gyroSensorManager.registerListener(gyroSensorEventListener, gyroSensor, SensorManager.SENSOR_DELAY_NORMAL);
		
		Card gyroCard = new Card(this);
		gyroCard.setText("GYRO");
		setContentView(gyroCard.toView());
	}
	
	@Override
	public boolean onKeyDown(int keycode, KeyEvent event) {
		if(keycode == KeyEvent.KEYCODE_BACK) {
			finish();
		}
		return false;
	}
	
	// event listener for gyro data
	private SensorEventListener gyroSensorEventListener = new SensorEventListener(){
		
		public void onSensorChanged(SensorEvent event){
			if(event.sensor.getType()== Sensor.TYPE_GYROSCOPE){
				
				long millis = System.currentTimeMillis() - startTime;
				int seconds = (int) millis/1000;
				if(seconds == 1) {
					getGyro(event);
					startTime = System.currentTimeMillis();
				}	
			} 
		}
		
		@Override
		public void onAccuracyChanged(Sensor arg0, int arg1) {
			// TODO Auto-generated method stub
		}

	};
	
	// Display the coordinates from gyroscope
	private void getGyro(SensorEvent event) {
		
		float axisX = event.values[0];
		float axisY = event.values[1];
		float axisZ = event.values[2];
		
		Card gyroCard = new Card(this);
		String coord = "X: " + axisX + "\n" + "Y: " + axisY + "\n" + "Z: " + axisZ;
		gyroCard.setText(coord);
		gyroCard.setFootnote("swipe down for home");
		
		setContentView(gyroCard.toView());
	}
}
