package com.abq.helloglass;

import java.util.ArrayList;
import java.util.List;

import com.google.android.glass.app.Card;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

public class CardScrollActivity extends Activity {
	
	private List<Card> cardList;
	private CardScrollView cardScroller;
	private GestureDetector gestDet;
	private String glassTag = "SCROLL_TAG";
	private Intent mainActIntent;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// create three cards
		createCards(3);
		
		// create card scroller 
		cardScroller = new CardScrollView(this);
		myCardScrollAdapter cardScrollAdapter = new myCardScrollAdapter();
		cardScroller.setAdapter(cardScrollAdapter);
		cardScroller.activate();
		
		// display 3 cards as card scroller
		setContentView(cardScroller);
		
		Log.v(glassTag, "SCROLL ON CREATE");
		
		// set intent for main activity
		// add reorder flag
		mainActIntent = new Intent(this, MainActivity.class);
		mainActIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // alternatively FLAG_ACTIVITY_CLEAR_TOP ... clears above activities
		//gestDet = createGestureDetector(this);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			Log.v(glassTag, "KEY BACK");
			startActivity(mainActIntent);
			finish();
			return true;
		}
		return false;
	}
	
	// gesture Detector
	private GestureDetector createGestureDetector(Context context) {
		
		Log.v(glassTag,"CREATE GESTURE");
		GestureDetector myGestDet = new GestureDetector(context);
		
		myGestDet.setBaseListener(new GestureDetector.BaseListener() {
			
			@Override
			public boolean onGesture(Gesture gest) {
				Log.v(glassTag,"ON GESTURE");
				Log.v(glassTag,gest.name());
				
				if(gest == Gesture.SWIPE_DOWN) {
					Log.v(glassTag, "SWIPE DOWN");
					//startActivity(menuActIntent);
					return true;
				}
				if(gest == Gesture.SWIPE_LEFT) {
					Log.v(glassTag, "SWIPE LEFT");
					//startActivity(menuActIntent);
					return true;
				}
				if(gest == Gesture.SWIPE_RIGHT) {
					Log.v(glassTag, "SWIPE RIGHT");
					//startActivity(menuActIntent);
					return true;
				}
				
				return false;
			}
		});
		
		myGestDet.setScrollListener(new GestureDetector.ScrollListener() {
            @Override
            public boolean onScroll(float displacement, float delta, float velocity) {
                Log.v(glassTag, "ON SCROLL");
                return true;
            }
        });
		
		return myGestDet;
	}
	
	@Override
    public boolean onGenericMotionEvent(MotionEvent event) {
		if (gestDet != null) {
            return gestDet.onMotionEvent(event);
        }
        return false;
    }
	
	// creates numberOfCards Cards
	private void createCards(int numberOfCards) {
		
		cardList = new ArrayList<Card>();
	
		for(int i=1; i<=numberOfCards; i++) {
			
			Card card = new Card(this);
			String cardText = "Card #" + i;
			card.setText(cardText);
			card.setFootnote("swipe down for main");
			cardList.add(card);
		}
	}
	
	// Adapter that allows you to scroll thru cards
	private class myCardScrollAdapter extends CardScrollAdapter {

		@Override
		public int findIdPosition(Object arg0) {
			return -1;
		}

		@Override
		public int findItemPosition(Object cardItem) {
			return cardList.indexOf(cardItem);
		}

		@Override
		public int getCount() {
			return cardList.size();
		}

		@Override
		public Object getItem(int position) {
			return cardList.get(position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return cardList.get(position).toView();
		}		
	}

}
