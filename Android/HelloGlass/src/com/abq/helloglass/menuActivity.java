package com.abq.helloglass;

import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class menuActivity extends Activity {
	
	String menuTag = "MENU_TAG";
	Intent mainActIntent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.v(menuTag, "MENU ON CREATE");
		
		openOptionsMenu();
		mainActIntent = new Intent(this, MainActivity.class);
		mainActIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			startActivity(mainActIntent);
			finish();
			Log.v(menuTag, "MENU BACK");
			return true;
		}
		return false;
	}
	
	// Inflates the menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		MenuInflater menuInfl = getMenuInflater();
		menuInfl.inflate(R.menu.menu_glass, menu);
		return true;
	}
	
	// determines which item is selected
	// and handles it
	public boolean onGroupItemClick(MenuItem menuItem) {
		
		int itemID = menuItem.getItemId();
		Intent scrollIntent = new Intent(this, CardScrollActivity.class);
		scrollIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		switch (itemID) {
			case R.id.firstItem:
				finish();
				startActivity(scrollIntent);
				return true;
			case R.id.secondItem:
				finish();
				startActivity(mainActIntent);
		}
		return false;
	}
}
