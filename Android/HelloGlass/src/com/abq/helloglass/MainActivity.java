package com.abq.helloglass;

import java.util.List;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.android.glass.app.Card;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;

public class MainActivity extends Activity{
	private SensorManager mSensorManager;
	private GestureDetector mGestureDetector;
	private String glassTag = "GLASS_TAG";
	private Sensor sensor;
	Card mainActivityCard;
	long startTime = System.currentTimeMillis();
	Intent menuIntent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		/*
		super.onCreate(savedInstanceState);
		helloWorldCard = new Card(this);
		helloWorldCard.setText("Hello Glass");
		Log.v(glassTag, "TAG GlASS");
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		if(sensor != null){
			Log.v(glassTag, "SENSOR NOT NULL");
			
			mSensorManager.registerListener(mySensorEventListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
			setContentView(helloWorldCard.toView());
		}
		else{
			Log.v(glassTag, "SENSOR NULL");
			helloWorldCard.setText("Hello, Danny + Oliver");
			setContentView(helloWorldCard.toView());
		} 
		*/
		super.onCreate(savedInstanceState);
		menuIntent = new Intent(this, menuActivity.class);
		menuIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		mainActivityCard = new Card(this);
		mainActivityCard.setText("Main");
		mainActivityCard.setFootnote("tap for menu");
		setContentView(mainActivityCard.toView());
		
		Log.v(glassTag, "MAIN ACT");
		
		mGestureDetector = createGestureDetector(this);
	}	
	
	// gesture detector
	private GestureDetector createGestureDetector(Context context) {
		
		Log.v(glassTag,"CREATE GESTURE");
		GestureDetector myGestDet = new GestureDetector(context);
		
		myGestDet.setBaseListener(new GestureDetector.BaseListener() {
			
			@Override
			public boolean onGesture(Gesture gesture) {
				Log.v(glassTag, "MAIN ON GEST");
				Log.v(glassTag, gesture.name());
				if(gesture == Gesture.TAP){
					startActivity(menuIntent);
					finish();
					Log.v(glassTag, "MAIN TAP");
				}
				if(gesture == Gesture.SWIPE_DOWN) {
					finish();
				}
				return false;
			}
		});
		
		return myGestDet;
	}
	
	// on generic motion gives the gesture detector
	// a motion to detect
	@Override
    public boolean onGenericMotionEvent(MotionEvent event) {
		Log.v(glassTag,"ON GENERIC MOTION");
        if (mGestureDetector != null) {
            return mGestureDetector.onMotionEvent(event);
        }
        return false;
    }
	
	/*
	private SensorEventListener mySensorEventListener = new SensorEventListener(){
		public void onSensorChanged(SensorEvent event){
			if(event.sensor.getType()== Sensor.TYPE_GYROSCOPE){
				//Log.v(glassTag, "LISTENER GYRO");
				long millis = System.currentTimeMillis() - startTime;
				int seconds = (int) millis/1000;
				if(seconds == 1) {
					getGyro(event);
					startTime = System.currentTimeMillis();
				}	
			} else {
				Log.v(glassTag, "LISTENER NOT GYRO");
			}
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub
			
		}
	};
	
	private void getGyro(SensorEvent event) {
		float axisX = event.values[0];
		float axisY = event.values[1];
		float axisZ = event.values[2];
		String coord = "x: " + axisX+"\n" + "y: " + axisY + "\n" + "z: "+ axisZ;
		Log.v(glassTag, coord);
		helloWorldCard.setText(coord);
		setContentView(helloWorldCard.toView());
	}
	*/
}
